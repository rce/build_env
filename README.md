# Build Environment

This project creates a docker image containing a build environment for cross compiling RCE software for armhf and arm64

Docker needs to be installed on the host system before using this package

# Creating a local docker container

`sh ./build.sh`

This creates a local docker container based on Ubuntu 20.04 LTS containing a cross compilation environment suitable for 
RCE ARM development. 

# Starting the docker container

`./build_env.sh`

This maps the user's home directory as well as UID/GID and starts a bash shell. 
