FROM ubuntu:focal
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=America/Los_Angeles
RUN apt-get update && apt-get install -y \
    apt-utils \
    curl \
    automake \
    autogen \
    bash \
    build-essential \
    u-boot-tools \
    libssl-dev \
    bc \
    bzip2 \
    ca-certificates \
    curl \
    cpio \
    file \
    git \
    gzip \
    make \
    ncurses-dev \
    pkg-config \
    libtool \
    python \
    rsync \
    sed \
    bison \
    flex \
    tar \
    vim \
    wget \
    runit \
    xz-utils \
    software-properties-common \
    sudo \
    cmake \
    binfmt-support \
    qemu-user-static \
    debootstrap \
    libbz2-dev \
    libbz2-dev  \
    gnuplot \
    python3-dev \
    gnupg

COPY etc/sudoers /etc/sudoers 
COPY etc/apt/sources.list /etc/apt/sources.list
COPY etc/profile.d/02-sethome.sh /etc/profile.d/02-sethome.sh
RUN apt-add-repository multiverse && apt-get update
RUN apt-get update
RUN apt-get install -y crossbuild-essential-armhf
RUN apt-get install -y crossbuild-essential-arm64

RUN dpkg --add-architecture armhf
RUN dpkg --add-architecture arm64
RUN apt-get update

# The cross-compiling emulator
RUN apt-get update && apt-get install -y \
    qemu-user \
    qemu-user-static
# install foreign archs for cross compilation
RUN apt-get install -y libbz2-dev:armhf libbz2-dev:armhf libzmq3-dev:armhf libpython3-dev:armhf
RUN apt-get install -y libbz2-dev:arm64 libbz2-dev:arm64 libzmq3-dev:arm64 libpython3-dev:arm64
